﻿<%@ Page Language='C#' AutoEventWireup='true' CodeBehind='SimpleDialog.aspx.cs' Inherits='AutoCloseDialog.SimpleDialog' %>

<!DOCTYPE html>

<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat='server'>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <title></title>
</head>
<body>
    <form id='form1' runat='server'>
    <style>
        .SDialogBox{
            width:550px; height:250px; background:#f5f5f5; border:1px solid #e3e3e3;
            padding:30px 40px; z-index:99999; position:relative; margin:30px auto; 
            /*position:fixed;*/
        }
        .SDialogBoxBody{
            line-height:1.8em; height:130px; font-weight:400;
        }
        .SDialogBoxBodyText{
            display:inline-block; vertical-align:top; padding-left:20px; width:450px; font-size:14px;
        }
        .SDialogBoxFooter{
            text-align:center; margin-top:10px;
        }
        .SDialogBoxFooter>.btnBlock{
            font-size:0.8em; cursor:pointer; background:#0078ad; color:#fff; font-weight:600;
            padding:8px 12px; display:inline-block; border-radius:3px;
        }
    </style>
        <div class='SDialogBox'>
            <div class='SDialogBoxBody'>
                <img style="width:56px; height:56px; border-radius:28px; background:#0078ad;" src="appbar.check.png"/>
                <div class="SDialogBoxBodyText">主题和样式也有助于文档保持协调。当您单击设计并选择新的主题时，图片、图表或 SmartArt 图形将会更改以匹配新的主题。当应用样式时，您的标题会进行更改以匹配新的主题。<br />
使用在需要位置出现的新按钮</div>
            </div>
            <div class='SDialogBoxFooter'>
                <div style='height:70px;padding-left:60px; font-weight:800; text-align:left; line-height:50px;font-size:1.1em; '>05秒后自动跳转……</div>
                <span class='btnBlock'>立即跳转</span>
            </div>
        </div>
        <script>
            function Alter(body, time) {
                setTimeout(function () {
                    alert('!');
                }, time ? time : 5000);
            }
            Alter('你好a', 5000);
        </script>

    </form>
</body>
</html>
