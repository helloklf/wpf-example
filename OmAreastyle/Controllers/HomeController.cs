﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OmArea.Areas.OmArea.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /OmArea/Home/

        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Start_V1000()
        {
            return View();
        }
    }
}
