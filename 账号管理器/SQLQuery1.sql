create database OMArea_MyAccount
go
use OMArea_MyAccount
go


create table AccountType
(
    --string typeGuid; //类型索引
    --string typeName; //类型名称
    --string userID;//创建者
    --int state; //类型状态
	typeGuid varchar(36) default(newID()) primary key ,
	typeName nvarchar(100),
	userID nvarchar(200),
	[state] int default(0)
)
go
create table Account
(
--string accountGUID; //记录索引号
--   string userID;//创建者

--   string siteUid;//账号
--   string alias;//别名
--   string sitePwd;//密码
--   string siteTitle;//名称
--   string siteUrl;//网址
--   string imagePath;//图片
--   string remark; //备注

	accountGUID varchar(36) default(newID()) primary key ,
	userID nvarchar(200),
	siteUid nvarchar(200),
	alias nvarchar(200),
	sitePwd nvarchar(200),
	siteTitle nvarchar(200),
	siteUrl nvarchar(200),
	imagePath nvarchar(200),
	remark nvarchar(500),
	[state] int default(0),
)

select * from Account