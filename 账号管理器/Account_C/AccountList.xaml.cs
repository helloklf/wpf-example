﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Account_C
{
    /// <summary>
    /// AccountList.xaml 的交互逻辑
    /// </summary>
    public partial class AccountList : Window
    {
        public AccountList()
        {
            InitializeComponent();
        }
        private void TextBlock_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Clipboard.SetText((sender as TextBlock).Text);
        }

        /// <summary>
        /// 浏览页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenSiteUrl_Click(object sender, RoutedEventArgs e)
        {
            var obj = sender as Hyperlink;
            if (obj != null && obj.NavigateUri != null && obj.NavigateUri.ToString().Length > 0)
            {
                System.Diagnostics.Process.Start(@"C:\Program Files\Internet Explorer\iexplore.exe", obj.NavigateUri.ToString());
            }
            else
                MessageBox.Show("您没有设置网址哦！");
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Clipboard.SetText(((sender as TextBlock).Tag as string) ?? "");
        }

        /// <summary>
        /// 右键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtenedNameList_Click(object sender, MouseButtonEventArgs e)
        {
            var data = (sender as Image).DataContext as Models.Account;
            Account_C.SiteAccountList sal = new SiteAccountList();
            sal.DataContext = data;
            Window w = new Window(); w.SizeToContent = SizeToContent.WidthAndHeight;
            w.WindowStyle = WindowStyle.ToolWindow;
            w.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            w.Content = sal;
            w.MinHeight = 50;
            w.MinWidth = 180;
            w.ShowDialog();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnEditing != null)
                Delegates.OnEditing((sender as Hyperlink).Tag as Models.Account);
            Close();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnDeleting != null)
                Delegates.OnDeleting((sender as Hyperlink).Tag as Models.Account);
            Close();
        }
    }
}
