﻿using Account_C.DB;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Account_C
{
    /// <summary>
    /// UpdateAccount.xaml 的交互逻辑
    /// </summary>
    public partial class UpdateAccount : UserControl
    {
        public UpdateAccount()
        {
            InitializeComponent();
            this.DataContext = new Models.Account() { SiteUrl="http://" };
        }
        public UpdateAccount(Models.Account accountInfo)
        {
            InitializeComponent();
            this.DataContext = accountInfo;
        }

        string newImage;
        /// <summary>
        /// 取消修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnEdited != null)
                Delegates.OnEdited(null);
        }

        /// <summary>
        /// 完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Complate_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists("HeaderImages"))
                Directory.CreateDirectory("HeaderImages");

            string oldImage = "";
            var account = this.DataContext as Models.Account;

            if (newImage != null && File.Exists(newImage))
            {
                FileInfo fi = new FileInfo(newImage);
                var filepath = "HeaderImages\\" + Guid.NewGuid().ToString() + fi.Extension;
                File.Copy(newImage, filepath);
                oldImage = account.ImagePath;
                account.ImagePath = filepath;
            }

            try
            {
                new DAL.DbOperate().UpdateAccount(account);
            }
            catch(Exception ex)
            {
                DbError.Update(ex);
            }
            if (Delegates.OnEdited != null)
                Delegates.OnEdited(account);
            else
            {
                MessageBox.Show("修改完成");
            }
            DataContext = new Models.Account() { SiteUrl = "Http://" };
            try
            {
                if (oldImage != null && File.Exists(oldImage))
                    File.Delete(oldImage);
            }
            catch (Exception ex) { MessageBox.Show("哇哇"); }
        }


        /// <summary>
        /// 更换图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectImage_Click(object sender, MouseButtonEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog of = new Microsoft.Win32.OpenFileDialog();
            of.Filter = "图片文件|*.png;*.jpg;*.gif;*.bmp;*.Jpeg|所有文件|*.*";
            if (of.ShowDialog() == true)
            {
                this.Xaml_SiteImg.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(of.FileName);
                newImage = of.FileName;
            }
        }
    }
}
