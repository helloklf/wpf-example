﻿
using System.Collections.Generic;
namespace Account_C
{
    public delegate void OnEdit(Models.Account accountInfo);
    public delegate void Reload();
    public delegate void Reloaded(List<Models.Account> accounts);
    public delegate void Search(string text);
    public static class Delegates 
    {
        /// <summary>
        /// 开始编辑
        /// </summary>
        public static OnEdit OnEditing;

        /// <summary>
        /// 编辑完成
        /// </summary>
        public static OnEdit OnEdited;

        /// <summary>
        /// 删除操作
        /// </summary>
        public static OnEdit OnDeleting;

        /// <summary>
        /// 删除完成
        /// </summary>
        public static OnEdit OnDeleted;

        /// <summary>
        /// 刷新列表
        /// </summary>
        public static Reload Reference;

        /// <summary>
        /// 刷新完毕
        /// </summary>
        public static Reloaded OnReferenced;

        /// <summary>
        /// 搜索
        /// </summary>
        public static Search Search;
        ///
    }
}
