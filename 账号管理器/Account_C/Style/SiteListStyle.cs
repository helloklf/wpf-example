﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Account_C
{
    public partial class SiteListStyle
    {
        public SiteListStyle()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 浏览页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenSiteUrl_Click(object sender, RoutedEventArgs e)
        {
            var obj = sender as Hyperlink;
            if (obj != null && obj.NavigateUri != null && obj.NavigateUri.ToString().Length>0)
            {
                System.Diagnostics.Process.Start(@"C:\Program Files\Internet Explorer\iexplore.exe", obj.NavigateUri.ToString());
            }
            else
                MessageBox.Show("您没有设置网址哦！");
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Clipboard.SetText(((sender as TextBlock).Tag as string)??"");
        }

        /// <summary>
        /// 右键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtenedNameList_Click(object sender, MouseButtonEventArgs e)
        {
            var data = (sender as Image).DataContext as Models.Account;
            new AccountList() { DataContext = data }.ShowDialog() ;
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnEditing != null)
                Delegates.OnEditing((sender as Hyperlink).Tag as Models.Account);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnDeleting != null)
                Delegates.OnDeleting((sender as Hyperlink).Tag as Models.Account);
        }
    }
}
