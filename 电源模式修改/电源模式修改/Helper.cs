﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 电源模式修改
{
    class Helper
    {

        static Microsoft.Win32.RegistryKey PowerSchemes = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Power\User\PowerSchemes");
        public static Dictionary<string, string> GetPowerSchemes()
        {
            Dictionary<string, string> powercfgs = new Dictionary<string, string>();
            foreach (var item in PowerSchemes.GetSubKeyNames())
            {
                switch (item)
                {
                    case "381b4222-f694-41f0-9685-ff5bb260df2e":
                        {
                            powercfgs.Add(item,"平衡");
                            break;
                        }
                    case "8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c":
                        {
                            powercfgs.Add(item, "高性能");
                            break;
                        }
                    case "a1841308-3541-4fab-bc81-f71556f20b4a":
                        {
                            powercfgs.Add(item, "节能");
                            break;
                        }
                    default :{
                            powercfgs.Add(item,PowerSchemes.OpenSubKey(item).GetValue("FriendlyName", "自定义模式").ToString());
                            break;
                        }
                }
            }
            return powercfgs;
        }

        public static string GetActivePowerScheme()
        {
            return PowerSchemes.GetValue("ActivePowerScheme","").ToString();
        }

        /// <summary>
        /// 开关自动电源管理
        /// </summary>
        /// <param name="enable"></param>
        public static void SetCsEnable(string enable)
        {
            var val = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Power\", "CsEnabled", "null").ToString();
            if (enable == val)
                return;
            else
                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Power\", "CsEnabled", int.Parse(enable));
        }
    }
}
