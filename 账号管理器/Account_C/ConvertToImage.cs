﻿using System;
using System.IO;
using System.Windows.Data;
using System.Windows.Media;

namespace Account_C
{
    public class ConvertToImage : IValueConverter
    {

        /// <summary>
        /// 将路径转换为图片
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && File.Exists(value.ToString()))
                {
                    using (FileStream fs = File.OpenRead(value.ToString()))
                    {
                        var bytes =new byte[fs.Length]; 
                        fs.Read(bytes,0,(int)fs.Length);
                        return new ImageSourceConverter().ConvertFrom(bytes);
                    }
                    //return new ImageSourceConverter().ConvertFromString(value.ToString());
                }

                if (File.Exists("Safari.png"))
                    return new ImageSourceConverter().ConvertFromString("Safari.png");
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
