﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dll_WPFControl
{
    /// <summary>
    /// UserOperate.xaml 的交互逻辑
    /// </summary>
    public partial class C_UserOperate_Black : UserControl
    {
        public C_UserOperate_Black()
        {
            InitializeComponent();

        }

        public string UserName { get; set; }
        public string UserPass { get; set; }
        public string UserEmali { get; set; }
        public int CodeLength { set { C_ResetPass.CodeLength = value; } get { return this.C_ResetPass.CodeLength; } }

        /// <summary>
        /// 登陆完成
        /// </summary>
        public event RoutedEventHandler OnLoginCompleted = null;
        /// <summary>
        /// 注册完成
        /// </summary>
        public event RoutedEventHandler OnRegisterCompleted = null;
        /// <summary>
        /// 密码重置完成
        /// </summary>
        public event RoutedEventHandler OnResetPassCompleted = null;

        /// <summary>
        /// 转到注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_White_OnGoRegister(object sender, RoutedEventArgs e)
        {
            this.Pages.SelectedIndex = 1;
        }

        /// <summary>
        /// 转到密码找回
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_White_OnGoResetPass(object sender, RoutedEventArgs e)
        {
            this.Pages.SelectedIndex = 2;
        }

        /// <summary>
        /// 转到登陆
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserRegister_White_OnGoLogin(object sender, RoutedEventArgs e)
        {
            this.Pages.SelectedIndex = 0;
        }

        /// <summary>
        /// 登陆完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_White_OnCompleted(object sender, RoutedEventArgs e)
        {
            UserName = this.C_Login.UserName;
            UserPass = this.C_Login.PassWord;
            if (OnLoginCompleted != null) { OnLoginCompleted(this,null); }
        }

        /// <summary>
        /// 注册完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserRegister_White_OnCompleted(object sender, RoutedEventArgs e)
        {
            UserName = this.C_Register.UserName;
            UserPass = this.C_Register.PassWord;
            UserEmali = this.C_Register.Email;
            if (OnRegisterCompleted != null) { OnRegisterCompleted(this, null); }
        }

        /// <summary>
        /// 密码重置完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_ResetPass_White_OnCompleted(object sender, RoutedEventArgs e)
        {
            UserName = this.C_ResetPass.UserName;
            UserPass = this.C_ResetPass.NewPassWord;
            if (OnResetPassCompleted != null) { OnResetPassCompleted(this, null); }
        }
    }
}
