﻿using System.Web.Mvc;

namespace OmArea.Areas.OmArea
{
    public class OmAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "OmArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "OmArea_default",
                "OmArea/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
