﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace 账号管理器
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Account_C.Delegates.OnEditing = new Account_C.OnEdit((a) => 
            {
                Xaml_Tabs.SelectedItem = Xaml_TItem_Edit;
                Xaml_TItem_Edit.Content = new Account_C.UpdateAccount(a) { Margin=new Thickness(10) };
            });

            Account_C.Delegates.OnEdited = new Account_C.OnEdit((a) =>
            {
                Xaml_Tabs.SelectedItem = Xaml_TItem_List;
            });

            Account_C.Delegates.OnReferenced = new Account_C.Reloaded((a) => 
            {
                this.DownSelectListBox.Data = (from item in a select item.SiteTitle).ToList();
            });
        }


        /// <summary>
        /// 拖动窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                }
                this.DragMove();
            }
        }


        /// <summary>
        /// 大小切换
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }


        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_Black_OnCompleted(object sender, RoutedEventArgs e)
        {
            var loginbox = (sender as Account_C.C_UserLogin_Black);
            var uid = "C7-CE-95-19-39-E7-73-F5-75-A1-74-C3-38-44-F6-25";//helloklf
            var pwd = "E1-A8-77-CE-6F-11-BA-1C-D4-33-FE-13-52-CF-38-1A";

            var md5 = System.Security.Cryptography.MD5.Create();
            var inputPwd = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(loginbox.PassWord.Trim())));

            if (loginbox.UserName.Trim().ToLower() == "helloklf" && inputPwd == pwd)
            {
                Xaml_TItem_List.Visibility = Visibility.Visible;
                Xaml_TItem_List.IsEnabled = true;
                DownSelectListBox.Visibility = Visibility.Visible;
                DownSelectListBox.IsEnabled = true;
                Xaml_Top_ReloadButton.Visibility = Visibility.Visible;
                Xaml_Top_ReloadButton.IsEnabled = true;
                Xaml_TItem_Add.Visibility = Visibility.Visible;
                Xaml_TItem_Add.IsEnabled = true;

                Xaml_TItem_Login.Visibility = Visibility.Collapsed;
                Xaml_Tabs.SelectedItem = Xaml_TItem_List;
            }
            else
            {
                Xaml_UserLoginState.Text = "该用户名不存在";
            }
        }


        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reference_Click(object sender, RoutedEventArgs e)
        {
            DownSelectListBox.Text = "";
            if (Account_C.Delegates.Search != null)
                Account_C.Delegates.Search("");
        }

        /// <summary>
        /// 导航到作者的个人网站
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenIE_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Program Files\Internet Explorer\iexplore.exe", (sender as Hyperlink).NavigateUri.ToString());
        }

        /// <summary>
        /// 双击窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Button_Click(null,null);
        }

        /// <summary>
        /// 搜索建议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownSelectListBox_Click(object sender, RoutedEventArgs e)
        {
            if(Account_C.Delegates.Search!=null)
                Account_C.Delegates.Search(DownSelectListBox.Text);
        }
    }
}
