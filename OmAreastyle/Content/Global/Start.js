﻿
var menu = $(".Navigate_Menu");
var navigatebar = $(".panle_NavigateBar");

function ResetSizeAndPostion() {
    $(".header").animate({ "font-size": parseInt(window.innerWidth / 15) }, 30); //.css("font-size", parseInt(window.innerWidth / 15) );
    $(".panel_Content").css("left", menu.width() + 40);
    navigatebar.css("left", menu.width() + 40);
    navigatebar.css("width", window.innerWidth - menu.width() - 65);
    $(".panel_Content").animate({ "width": window.innerWidth - menu.width() - 65, "height": window.innerHeight - 115 }, 10);
}

$(this).resize(ResetSizeAndPostion).load(ResetSizeAndPostion);

$(".Navigate_Menu li").on("click", function ($e) {
    var action = $e.currentTarget.attributes["data-action"].value;
    var controler = $e.currentTarget.attributes["data-controler"].value;

    if ($("#Html_MainContent").attr("data-action") == action && $("#Html_MainContent").attr("data-controler") == controler)
        return;

    $("#Html_MainContent").attr("data-action", action);
    $("#Html_MainContent").attr("data-controler", controler);
    AjaxGetPage(controler, action);
});

//参数1 控制器名称， 参数2 action名称 ， 参数3 页面显示容器的id（默认为Html_MainContent）
function AjaxGetPage(controler, actionName, panel) {

    if (!panel)
        panel = "Html_MainContent";

    var $panel = $("#" + panel);

    if (!controler)
        controler = $panel.attr("data-controler");
    if (!actionName)
        actionName = $panel.attr("data-action");

    if (actionName) {
        $panel.slideUp(100);
        try {
            $.ajax({
                url: "/" + controler + "/" + actionName,
                cache: false,
                success: function (response) {
                    if (panel)
                        $panel.html(response);
                    $panel.slideDown(1000);
                },
                error: function (eData) {
                    alert(eData);
                    if (panel)
                        $panel.html(eData.responseTex);
                    $panel.slideDown(1000);
                }
            });
        }
        catch (e) {
            $panel.slideDown(1000);
        }
    }
}

$("#ReloadBtn").on("click", function ($e) {
    AjaxGetPage();
})
$(
    function () {
        setInterval(function () {
            $("#SystemTime").html(Date());
        }, 1000)
    }
);