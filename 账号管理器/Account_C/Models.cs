﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Models
{
    public class AccountType
    {
        string typeGuid; //类型索引
        string typeName; //类型名称
        int state; //类型状态
        string userID;//创建者

        /// <summary>
        /// 创建者
        /// </summary>
        public string UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        public string TypeGuid
        {
            get { return typeGuid; }
            set { typeGuid = value; }
        }


        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }


        public int State
        {
            get { return state; }
            set { state = value; }
        }
    }

    public class Account : AccountType, INotifyPropertyChanged
    {
        string accountGUID; //记录索引号

        string siteUid;//账号
        string alias;//别名
        string sitePwd;//密码
        string siteTitle;//名称
        string siteUrl;//网址
        string imagePath;//图片
        string remark; //备注


        /// <summary>
        /// 记录索引
        /// </summary>
        public string AccountGUID
        {
            get { return accountGUID; }
            set { Change("AccountGUID"); accountGUID = value; }
        }

        /// <summary>
        /// 账号
        /// </summary>
        public string SiteUid
        {
            get { return siteUid; }
            set { Change("SiteUid"); Change("SiteUidText"); siteUid = value; }
        }
        
        /// <summary>
        /// 账号（文本，只读）
        /// </summary>
        public string SiteUidText
        {
            get { return "🔑 " + siteUid??""; }
        }

        /// <summary>
        /// 别名（以”；“分隔的所有别名）
        /// </summary>
        public string Alias
        {
            get { return alias; }
            set { Change("Alias"); alias = value; }
        }
        /// <summary>
        /// 别名集合
        /// </summary>
        public List<string> AliasList
        {
            get 
            {
                return Alias==null?new List<string>():Alias.Split(';').ToList();
            }
            set 
            {
                if (value == null)
                    return;
                Alias = string.Join(";", value);
            }
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string SitePwd
        {
            get { return sitePwd; }
            set { Change("SitePwd"); Change("SitePwdText"); sitePwd = value; }
        }

        /// <summary>
        /// 密码（文本，只读）
        /// </summary>
        public string SitePwdText
        {
            get { return "🔒 " + sitePwd ?? ""; }
        }

        /// <summary>
        /// 标题
        /// </summary>
        public string SiteTitle
        {
            get { return siteTitle; }
            set { Change("SiteTitle"); siteTitle = value; }
        }

        /// <summary>
        /// 标题（文本，只读）
        /// </summary>
        public string SiteTitleText
        {
            get { return "" + siteTitle; }
        }

        /// <summary>
        /// 网址
        /// </summary>
        public string SiteUrl
        {
            get { return siteUrl; }
            set { Change("SiteUrl"); siteUrl = value; }
        }

        /// <summary>
        /// 图片路径
        /// </summary>
        public string ImagePath
        {
            get 
            {
                return imagePath;
            }
            set { Change("ImagePath"); imagePath = value; }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get { return remark; }
            set { Change("Remark"); remark = value; }
        }

        public void Change(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,new PropertyChangedEventArgs(name));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
