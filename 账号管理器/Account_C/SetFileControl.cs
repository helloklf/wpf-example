﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace Account_C
{
    public static class SetFileControl
    {
        /// <summary>
        /// 设置文件访问权限为完全控制
        /// </summary>
        /// <param name="files"></param>
        /// <param name="userName"></param>
        public static void SetFilesControl(string[] files, string userName = "Everyone")
        {
            foreach (var item in files)
            {
                if(System.IO.File.Exists(item))
                {
                    FileInfo fif = new FileInfo(item);
                    var ac = fif.GetAccessControl();
                    ac.SetAccessRule(new FileSystemAccessRule(userName??"Everyone", FileSystemRights.FullControl, AccessControlType.Allow));
                    fif.SetAccessControl(ac);
                }
            }
        }
    }
}
