﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace 电源模式修改
{
    /// <summary>
    /// AdvancePower.xaml 的交互逻辑
    /// </summary>
    public partial class AdvancePower : Window
    {
        public AdvancePower()
        {
            InitializeComponent();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        string file = AppDomain.CurrentDomain.BaseDirectory + "energy-report.html";
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
            PowerCfgCMD.ExecCommand("energy", new OnOutput((a) =>
            {
                if (a.IndexOf("energy-report.html") > -1)
                {
                    this.Dispatcher.Invoke(() => ShowReport());
                }
                this.Dispatcher.Invoke(() => Xaml_CommandOutputText.Text += a + "\r\n");
            }), new OnExit(() =>
            {
                if (System.IO.File.Exists(file))
                {
                    this.Dispatcher.Invoke(() => ShowReport());
                }
            }));
        }

        void ShowReport()
        {
            Window w = new Window();
            w.Title = "报告信息" + DateTime.Now.ToLongTimeString();
            w.Content = new WebBrowser() { Source = new Uri(file, UriKind.Absolute) };
            w.Show();
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Xaml_CommandOutputText_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// 飞行模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.Content = "✈";
        }

        /// <summary>
        /// 亮度模式
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.Content = "☀";
        }

        /// <summary>
        /// 定时关机
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn.Content = "⏳";
        }
    }
}
