﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dll_WPFControl
{
    /// <summary>
    /// UserLogin.xaml 的交互逻辑
    /// </summary>
    public partial class C_UserLogin_Black : UserControl
    {
        public C_UserLogin_Black()
        {
            InitializeComponent();
        }

        
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get { return this.Xaml_UserName.Text; }
            set
            {
                this.Xaml_UserName.Text = value;
                if (value != null && value.Length > 1) { this.Xaml_UserError.Visibility = Visibility.Collapsed; }
            }
        }
        /// <summary>
        /// 密码文本
        /// </summary>
        public string PassWord
        {
            get { return this.Xaml_UserPass.Text.Trim(); }
        }

        /// <summary>
        /// 登陆完成时的操作
        /// </summary>
        public event RoutedEventHandler OnCompleted = null;


        /// <summary>
        /// 点击跳转到注册页面的操作
        /// </summary>
        public event RoutedEventHandler OnGoRegister = null;

        /// <summary>
        /// 点击找回密码时的操作
        /// </summary>
        public event RoutedEventHandler OnGoResetPass = null;

        /// <summary>
        /// 用户名输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_UserError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 用户名输入框丢失焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length == 0)
                Xaml_UserError.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 密码输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_PassError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 密码输入框焦点丢失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserPass.Text.Length == 0)
                Xaml_PassError.Visibility = Visibility.Visible;
        }
        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            ErrorInfo.Text = "";
        }

        /// <summary>
        /// 登陆点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length < 5)
            {
                ErrorInfo.Text = "用户名至少需要5位";
            }
            else if (Xaml_UserPass.Text.Length < 6)
            {
                ErrorInfo.Text = "用户密码至少6位";
            }
            else 
            {
                if (OnCompleted != null) { OnCompleted(this, null); }
            }
        }

        /// <summary>
        /// 跳转到新用户注册界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Go_AddNewUser(object sender, RoutedEventArgs e)
        {
            if (OnGoRegister != null) { OnGoRegister(this,null); }
        }

        /// <summary>
        /// 条状到密码找回页面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Go_ResetPass(object sender, RoutedEventArgs e)
        {
            if (OnGoResetPass != null) OnGoResetPass(this,null);
        }

    }
}
