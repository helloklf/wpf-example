﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_WP8风格
{
    /// <summary>
    /// Metro_Tab_White.xaml 的交互逻辑
    /// </summary>
    public partial class Metro_Tab_White : UserControl
    {
        public Metro_Tab_White()
        {
            InitializeComponent();
        }


        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
