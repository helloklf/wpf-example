﻿using System.Windows;
using System.Windows.Controls;

namespace Account_C
{
    /// <summary>
    /// SiteAccountList.xaml 的交互逻辑
    /// </summary>
    public partial class SiteAccountList : UserControl
    {
        public SiteAccountList()
        {
            InitializeComponent();
        }

        private void TextBlock_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Clipboard.SetText((sender as TextBlock).Text);
        }
    }
}
