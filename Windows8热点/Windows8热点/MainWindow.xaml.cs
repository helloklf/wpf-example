﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;

namespace Windows8热点
{

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DispatcherTimer time = new DispatcherTimer();
            time.Tick += (a, b) =>
            {
                ServiceGet();
                ServiceState();
            };
            time.Interval = TimeSpan.FromSeconds(30);
            time.Start();
        }

        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        /// <summary>
        /// 启动wifi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wifi_Start(object sender, RoutedEventArgs e)
        {
            if (Xaml_WifiPwd.Text.Length < 8)
            {
                MessageBox.Show("WIFI密码至少需要8个字符");
                return;
            }
            if (Xaml_ServiceIsAutoRun.IsChecked == false)
            {
                StartWifi();
            }
            else
            {
                ServiceOpen();
            }
        }

        public delegate void OnOutput(string text);

        void StartWifi()
        {
            UpdateConfig();
            Xaml_ConsoleOutput.Text = "";

            WifiConsole(string.Format(" wlan set hostednetwork mode=allow ssid={0} key={1}", this.Xaml_WifiName.Text, this.Xaml_WifiPwd.Text), (a) =>
            {
                if (a.Contains("已成功更改托管网络的用户密钥密码"))
                {
                    WifiConsole("wlan start hostednetwork");
                }
            });
        }

        void StopWifi()
        {
            WifiConsole("wlan stop hostednetwork");
        }

        
        void WifiConsole(string param,OnOutput onoutput = null)
        {
            var process = new System.Diagnostics.ProcessStartInfo("netsh");
            process.Arguments = param;
            process.RedirectStandardInput = true;
            process.RedirectStandardOutput = true;
            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            var proc = System.Diagnostics.Process.Start(process);
            proc.OutputDataReceived += proc_OutputDataReceived;
            if (onoutput != null)
                proc.OutputDataReceived += (a, b) => { if(!string.IsNullOrWhiteSpace(b.Data)) onoutput(b.Data); };
            proc.BeginOutputReadLine();
        }


        /// <summary>
        /// 输出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void proc_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.Data))
            {
                this.Dispatcher.Invoke( new Action(()=> Xaml_ConsoleOutput.Text += e.Data+"\r\n") );
            }
        }

        void ServiceSetUp(string Arguments="")
        {
            UpdateConfig();

            string FileName = "Windows8Wifi_Service.exe";
            if (File.Exists(FileName))
            {
                //MessageBox.Show(System.IO.Path.GetFullPath(FileName));
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(Environment.SystemDirectory+"\\..\\Microsoft.NET\\Framework\\v4.0.30319\\Installutil.exe ");
                info.Arguments = System.IO.Path.GetFullPath(FileName) + " " + Arguments;
                info.RedirectStandardInput = true;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                Process process = Process.Start(info);

                Window w = new Window(); w.Content = "";
                w.Title = "正在安装基础服务，请在提示“完成”或“失败”后关闭此窗口！";
                process.OutputDataReceived += (a, b) =>
                {
                    if (string.IsNullOrWhiteSpace(b.Data))
                        return;
                    this.Dispatcher.Invoke( new Action( () => { w.Content += (b.Data + "\r\n"); }));
                };
                process.BeginOutputReadLine();
                w.ShowDialog();
            }
            else
            {
                MessageBox.Show("服务文件不存在，无法安装到系统系统服务！");
            }
        }


        /// <summary>
        /// 开启托管服务
        /// </summary>
        void ServiceOpen()
        {
            UpdateConfig();
            var service = ServiceGet();
            if (service == null)
            {
                ServiceSetUp("");
            }
            if (service.Status == ServiceControllerStatus.Stopped)
                service.Start(); //启动服务
            this.Xaml_ConsoleOutput.Text = "请稍等片刻后点击窗口顶部的刷新按钮检查操作结果";
            
        }

        void UpdateConfig()
        {
            var oldData =LoadConfig();
            if (oldData == null || oldData[0] != this.Xaml_WifiName.Text.Trim() || oldData[1] != this.Xaml_WifiPwd.Text.Trim())
            {
                XElement name = new XElement("add", new XAttribute("key", "WifiName"), new XAttribute("value", this.Xaml_WifiName.Text.Trim()));
                XElement pwd = new XElement("add", new XAttribute("key", "WifiPassWord"), new XAttribute("value", this.Xaml_WifiPwd.Text.Trim()));
                XElement xe = new XElement("configuration", new XElement("appSettings", name, pwd));
                xe.Save("Windows8Wifi_Service.exe.config");
            }
        }

        string[] LoadConfig()
        {
            try
            {
                if (File.Exists("Windows8Wifi_Service.exe.config"))
                {
                    XElement xe = XElement.Load("Windows8Wifi_Service.exe.config");
                    var name = xe.Element("appSettings").Elements("add").First().Attribute("value").Value;
                    var pwd = xe.Element("appSettings").Elements("add").Last().Attribute("value").Value;
                    return new string[] { name, pwd };
                }
                return null;
            }
            catch {
                return null;
            }
        }


        /// <summary>
        /// 关闭托管的服务
        /// </summary>
        void ServiceClose()
        {
            var service = ServiceGet();
            if (service == null)
                return;
            if (service.CanStop)
                service.Stop(); //关闭服务
            this.Xaml_ConsoleOutput.Text = "请稍等片刻后点击窗口顶部的刷新按钮检查操作结果";
        }

        /// <summary>
        /// 获取托管服务
        /// </summary>
        ServiceController ServiceGet()
        {
            //WifiAutoStart
            var servicename = "WifiAutoStart";
            ServiceController[] services = ServiceController.GetServices();

            foreach (var item in services)
            {
                if (item.ServiceName == servicename)//判断服务是否安装
                {
                    Xaml_ServiceIsAutoRun.IsChecked = true;
                    return item;
                }
                else
                    Xaml_ServiceIsAutoRun.IsChecked = false;
            }
            return null;
        }

        void ServiceState()
        {
            var service =ServiceGet();
            this.Xaml_ConsoleOutput.Text =service==null?"未安装自启动服务":"托管服务状态："+service.Status;
        }

        /// <summary>
        /// 重启服务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Click_Restart(object sender, RoutedEventArgs e)
        {
            if (Xaml_ServiceIsAutoRun.IsChecked == false)
            {
                StopWifi();
                StartWifi();
            }
            else
            {
                ServiceClose();
                ServiceOpen();
            }
        }

        /// <summary>
        /// 关闭wifi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wifi_Close(object sender, RoutedEventArgs e)
        {
            if (Xaml_ServiceIsAutoRun.IsChecked == false)
            {
                StopWifi();
            }
            else
            {
                ServiceClose();
            }
        }

        /// <summary>
        /// 自启动Wifi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoStart_Click(object sender, RoutedEventArgs e)
        {
            ServiceSetUp(ServiceGet() == null ? "" : "/u");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var data = LoadConfig();
            if (data != null && data.Length == 2)
            {
                this.Xaml_WifiName.Text = data[0];
                this.Xaml_WifiPwd.Text = data[1];
            }
            //ServiceOpen();
            
            
            /*ServiceController cs = new ServiceController(); 
            cs.MachineName = "localhost "; //主机名称
            cs.ServiceName = "Messenger "; //服务名称
            cs.Refresh(); 
            if (cs.Status == ServiceControllerStatus.Running) { 
                //判断已经运行
            } 
             
            补充一下，获取服务名的方法：
            public bool ServiceIsExisted(string serviceName)   
            {
                serviceController[] services = ServiceController.GetServices();  
                foreach (ServiceController s in services)  
                { 
                    if (s.ServiceName == serviceName)  
                    { 
                        return true;   }   }   return false;   
            } */
        }

        private void Reload_Click(object sender, RoutedEventArgs e)
        {
            ServiceGet();
            ServiceState();
        }

        private void Xaml_WifiName_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }
    }
}
