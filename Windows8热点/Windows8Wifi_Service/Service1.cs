﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Windows8Wifi_Service
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            StartWifi();
        }

        protected override void OnStop()
        {
            StopWifi();
        }


        void StartWifi()
        {
            var name = System.Configuration.ConfigurationSettings.AppSettings["WifiName"] ?? "ThisWifi";
            var pwd = System.Configuration.ConfigurationSettings.AppSettings["WifiPassWord"] ?? "1234567890";
            WifiConsole(string.Format(" wlan set hostednetwork mode=allow ssid={0} key={1}", name, pwd), (a) =>
            {
                if (a.Contains("已成功更改托管网络的用户密钥密码"))
                {
                    WifiConsole("wlan start hostednetwork");
                }
            });
        }

        void RestartWifi()
        {
            //Xaml_ConsoleOutput.Text = "";
        }

        void StopWifi()
        {
            WifiConsole("wlan stop hostednetwork");
        }


        public delegate void OnOutput(string text);

        void WifiConsole(string param, OnOutput onoutput = null)
        {
            var process = new System.Diagnostics.ProcessStartInfo("netsh");
            process.Arguments = param;
            process.RedirectStandardInput = true;
            process.RedirectStandardOutput = true;
            process.CreateNoWindow = true;
            process.UseShellExecute = false;
            var proc = System.Diagnostics.Process.Start(process);
            proc.OutputDataReceived += proc_OutputDataReceived;
            if (onoutput != null)
                proc.OutputDataReceived += (a, b) => { if (!string.IsNullOrWhiteSpace(b.Data)) onoutput(b.Data); };
            proc.BeginOutputReadLine();
        }


        /// <summary>
        /// 输出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void proc_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.Data))
            {

                //this.Dispatcher.Invoke(new Action(() => Xaml_ConsoleOutput.Text += e.Data + "\r\n"));
            }
        }

    }
}
