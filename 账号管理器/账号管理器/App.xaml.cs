﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace 账号管理器
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            if (!new DAL.DbOperate().TestDbConnection())
            {
                DAL.DbConnects.CreateConn(ConfigurationManager.ConnectionStrings["SqlExpress"].ConnectionString);
                throw new Exception("当前数据库配置不正确。无法连接到数据提供程序，应用程序无法正常工作，即将退出！");
            }
            else
            {
                //复制到新的数据库
                //new DAL.DbOperate().CopyToNewDB(ConfigurationManager.ConnectionStrings["LocalDB"].ConnectionString);
            }
        }
    }
}
