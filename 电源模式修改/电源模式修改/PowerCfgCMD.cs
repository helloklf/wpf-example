﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 电源模式修改
{
    public delegate void OnOutput(string text);
    public delegate void OnExit();
    public class PowerCfgCMD
    {
        public static void ExecCommand(string cmdText,OnOutput output = null,OnExit onexit = null)
        {
            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo("powercfg");
            processStartInfo.Arguments = cmdText;
            processStartInfo.RedirectStandardInput = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = true;

            System.Diagnostics.Process pro = System.Diagnostics.Process.Start(processStartInfo);
            //pro.StandardInput.WriteLine("exit");
            pro.Exited += (sender, e) =>
            {
                if (onexit != null)
                {
                    onexit();
                }
            };
            pro.OutputDataReceived += (a, b) =>
            {
                if (string.IsNullOrWhiteSpace(b.Data))
                    return;
                if (output != null)
                {
                    output(b.Data);
                }
            };
            pro.BeginOutputReadLine();

            //pro.StandardInput.WriteLine("chcp 437");
            //pro.StandardInput.WriteLine(cmdText);
            //pro.StandardInput.WriteLine("exit");
            //string result = pro.StandardOutput.ReadToEnd();
            //return result;
        }

    }
}
