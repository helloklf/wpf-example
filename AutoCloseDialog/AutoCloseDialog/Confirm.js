﻿//Base64图片数据：警告（三角形+感叹号）
var Icon_Warning = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAYAAADHl1ErAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAOrSURBVHhe7do/aFNBHAfwJqitBtSC7aQitJu4FNpBwUXFoVAd1MnBQQTFSSguHUJRwcXFyQ7iKE7+QRxUgoOICuJSXAoORQhRIqZL0uaP38t9E5LeS/LevZfk5eX3gZB3f3y9++XXe3eNI0IIIYQQQgghhBChEuN7aGSz2X3j4+OXyuXy0Xg8/qdQKDwdGxtbY7NoVCwWr1Yqlb941SFwxVKpdJ9dRM3m5uYcY+QIwZxnV6Egk94wNo7Q/oVdhcoexqUtyTJIJpNxlT2MSVuSZYCsucJ4uDLUWba6uroLWbPGWLgy1FmG7cIi4+DJUGZZOp1OIFsyjIEnQ5llmPddPX07/cqyvhyNNjY2JhOJxM9YLLaHVSqAH3jpZCf6Hud1Ffp/xdFpjsVow2RXVJZss5fNBrTN6C7NkGXn2SW68vn8NNagIufcaIZdDGhb0F2aqSdsKpXawW7RhEk+53y3W2AXA56mt9jHoPZx7NYTcb73hDpgYy06x2ITBHKalwasVQd5aUDbUmSzDEH5yMRw8oDdDGh7prs463WW9YTaBnB+rbxgVwMC/Yl9HEVuLVOTwaR+cH6O0P6N3Q1oXte9WotUlqnJcF7t/GP3Jqgf1c3tRSbLeAT6xXl1YuzF1DaEbR1FIss8HrCNvRjqzuqmzgY+y3K53AFMwssB29iLoe6ybnKn21nW1X0Yzou3se+aYLEjBNfYi6HuEC9dUfsyxG03i4HrWsAw6MMI1g0WXXHaoKJuipeu4GdOIcg3WQxc1wKGQS9j8PW/RrjkFJyWu/xW8HMX1cOGxfArFArHEDCnA3Zb+DfGXgzVnv6EXaMeNrxF+GHirzlurzK8RR3q8rrJG4whMxBZhrGe1EPuv4HIMnyyrr5j7IVuZFmgiz72QBew4M6yaOszJrqE92t4va3WWMJYJsDTk7pneMC2WqBr8Cv0RH0TzltWoe4Rm62Edi3DxK5zjH7s5+3q1GmBbdZCt5bxgG31HWODdd7OoNp0FzuhyzKMSR1H/Mo7TUrVsd0XZNkyb9lfGMssPkHPm1QnmNTj7WsYqh/qVn8wxvLW1tYJ3taa7y9yMY53eBqdYjEI6in5CmdItYm9iNeZam0AELf3uO9pFq34DhgGkcfbqC6F3m98uJO8thLEPmxQgqX4HqvvgKk05+Ug+M53a74DhoX6HoJWYjG0MMYK3u7okr1A/vcOjkTzWBuOsBhKWOzXMcaXLAohhBBCCCGEEEIIIYQQItJGRv4DxKQdJi6kmaYAAAAASUVORK5CYII='/></td><td><div class='SConfirmBoxBodyText";

//Base64图片数据：关闭（“X”按钮）
var Icon_Close = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAYAAADHl1ErAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFkSURBVHhe7dpBToNAFIDhYmDhLVx4EPduXLk2HsHUe3kEL6ULYaxvZF5iqLUzMBhm3v8lhAyhNO9vStKUHQAAAAAAAAAAAPB/hmF4lO0uLLPp+/7GOfcUlnXwsT5HHzmj+VhyzbfD6DkcLpvGCkMdckWbxFJlR5vGUkujnYilyowmQR5+i6XmRjsT65vc0/bh9HLEDJYabY1rbkrOAauPpXIMaiaWWjKwuVhqzuBmY6mUAOZjqdgQxPohJtpfTMVSc6OZjKVSo5mOpWKjbSXWRdijBHwlE3DTTzA3ljIVLSaWDxJzTvXRYmP5ECnnhsvXZU4As9GWDG4uWo6BzUTLOWj10dYYcAvRVvtp1DTNlewux9Uxma2X4e7btn0Jh87quu7VOXcrr30Ph47I+3ayXYdlWeST5o/cVNNoS2OpE9Hqer5CZL2/TKLVEUv5aDljKR+tusedAAAAAAAAAAAAsGm73Rda1br6wfcBoQAAAABJRU5ErkJggg==";

//Base64图片数据：确定（“✅”按钮）
var Icon_Check = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAYAAADHl1ErAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAI6SURBVHhe7dg/SBthGMfxIKUgpSCCk0uHUuji6Ojm0snFzbVTly4d2ilbcXN2cHNxF8dQR3GTgJtTIW2GEgohmH9+37vnIKnvXXImFd57fx843ph77vXeH7k77qmJiIiIiIiIiIiIiIiIVMt4PN4YDoeH9Xp9xb6SPM1m8yWB/WAbE9qJQpuBkI5cWBmFVoB89tOYpik0D3LZYusmCXm40Kx0KYJOv91uv2Y4Y1tNvvD7ZWPc3KU2Go0u7IeU57zRaLywQ+JGGN/STPwI84ZhzcrjRhAfCGSQJOP3u9frvbXyuBHGG7Y/SSweBHnPsGvlcWu1Wq8I5CqNxo+n4icrD487eS6Nd/bnwpjv1HLJc2yl4eHkv7gV8Iv4uYzQmOqjmy8P/+fSvR5ZeVg4/ySszKKh9fv9Hbs35bnrdDrrVh4WTn4qrMxTQ+PQDXdsOstj7PvL8N7Kw8KJe8PKlA1tsgPhw3yjwWCwZ+XhYQ2FgTllQuMmP9WB+Bf7v1ppuFjHUkKjzNuBmODeIauBxSwUGrsLOxAce81Q9MIdHhb0pNBcB4Jdt2nFY+6Ybre7aeXVwvpKhTarA8G+e2wnk1cV65w7ND4WdiBwYNNWGwudGRpaBJfbgeCJ+N2miwNrnie0PHE2All46dD41cXdCGTxZUJTI9AhiHkeBGoETiKMwtBcL81KJUMueaGF2wj83whnKjQuxXAbgc+FnLLQwm0EPjfuWZ8JLMxGoIiIiIiIiIiIiIiIiESlVnsAVoe0Wymm6WkAAAAASUVORK5CYII=";

function ConfirmMini(text, postback) {
    var Result = false;
    var blockId = randomChar(10);
    function randomChar(l) { //随机字符串创建器
        var x = "123456789poiuytrewqasdfghjklmnbvcxzQWERTYUIPLKJHGFDSAZXCVBNM";
        var tmp = "";
        for (var i = 0; i < l; i++) {
            tmp += x.charAt(Math.ceil(Math.random() * 100000000) % x.length);
        }
        return tmp;
    }
    function OpenConfirm() {
        if (document.body) document.body.innerHTML += HtmlContent;
        else document.write(HtmlContent);
        document.getElementById(blockId + "_yes").onclick = function () { Result = true; CloseConfirm(); };
        document.getElementById(blockId + "_no").onclick = CloseConfirm;
        document.getElementById(blockId + "_close").onclick = CloseConfirm;
    }
    function CloseConfirm() {
        var block = document.getElementById(blockId);
        block.parentNode.removeChild(block);
        if (postback) postback(Result); //执行回送方法
    }

    var HtmlContent = "<div id='" + blockId + "' class='MiniConfirmBox'><style>.MiniConfirmBox{border:1px solid#e5e5e5;display:inline-block;width:auto;min-width:200px;background:#f5f5f5;max-width:300px;position:fixed;z-index:99998;top:20%;left:40%;}.SConfirmBoxBodyText{line-height:1.8em;font-weight:400;font-weight:600;display:inline-block;vertical-align:top;padding:10px;font-size:14px;}.SConfirmBoxBodyImg{width:32px;margin:10px;height:32px;border-radius:16px;background:#d0a000}.SConfirmBoxClosseBlock{font-size:9px;cursor:pointer;color:#fff;border:1px solid#f5f5f5;background:#e5e5e5;font-weight:600;line-height:20px;text-align:center;width:18px;height:18px;display:inline-block;border-radius:2px;vertical-align:top;}.SConfirmBoxButton{font-size:9px;cursor:pointer;color:#fff;background:#e5e5e5;font-weight:600;line-height:25px;text-align:center;width:35px;height:25px;margin-right:5px;display:inline-block;border-radius:2px;vertical-align:top;}.SConfirmBoxButton img{max-width:25px;max-height:25px;}.btnBlock:hover{background:#c5c5c5;}</style><table style='width:100%; border-collapse:collapse; margin:3px 0px;'><tr><td style='width:auto; vertical-align:top'><img class='SConfirmBoxBodyImg'src='" + Icon_Warning + "'>" + (text ? text : "确定?") + "</di></td><td style='vertical-align:top; width:22px; padding:0px;'><img id='" + blockId + "_close' class='SConfirmBoxClosseBlock'src='" + Icon_Close + "'/></td></tr><tr><td colspan='3'style='text-align:right; '><span class='SConfirmBoxButton'style='background:#0078ad' id='" + blockId + "_yes'><img src='" + Icon_Check + "'/></span><span' id='" + blockId + "_no' class='SConfirmBoxButton'style='background:#800000;'><img src='" + Icon_Close + "'/></span></td></tr></table></div>";
    OpenConfirm();
}