﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dll_WPFControl
{
    /// <summary>
    /// C_ResetPass_Black：黑色文本的密码重置框
    /// </summary>
    public partial class C_ResetPass_Black : UserControl
    {
        public C_ResetPass_Black()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 验证码长度
        /// </summary>
        public int CodeLength { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get { return this.Xaml_UserName.Text; }
            set
            {
                this.Xaml_UserName.Text = value;
                if (value != null && value.Length > 1) { this.Xaml_UserError.Visibility = Visibility.Collapsed; }
            }
        }
        /// <summary>
        /// 新密码文本
        /// </summary>
        public string NewPassWord
        {
            get { return this.Xaml_NewPass.Text.Trim(); }
        }

        /// <summary>
        /// 密码修改完成时的操作
        /// </summary>
        public event RoutedEventHandler OnCompleted = null;

        /// <summary>
        /// 发送验证码
        /// </summary>
        public event RoutedEventHandler OnSendCode = null;

        string Code;

        /// <summary>
        /// 验证码
        /// </summary>
        public string CodeText
        {
            get { return Code; }
        }

        /// <summary>
        /// 验证码验证
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text == "")
            {
                ErrorInfo.Text = "您还未输入用户名";
            }
            else if (Xaml_Code.Text == "")
            {
                ErrorInfo.Text = "还未输入验证码";
            }
            else
            {
                CodeIsValid();
            }
        }

        void CodeIsValid()
        {
            //这里写验证码验证功能
            if (Code == Xaml_Code.Text)
            {
                GoInputNewPassState();
            }
            else
            {
                ErrorInfo.Text = "验证码输入错误";
            }
        }


        /// <summary>
        /// 新密码提交
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Xaml_Sub_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_NewPass.Text.Length > 5)
            {
                if (OnCompleted != null)
                    OnCompleted(this, null);
            }
            else
            {
                ErrorInfo.Text = "密码长度过短（至少六位）";
            }
        }

        /// <summary>
        /// 获得验证码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetCode(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length < 5)
            {
                ErrorInfo.Text = "还未输入有效用户名";
            }
            else
            {
                Code = CreateEnglish(CodeLength < 1 ? 4 : CodeLength);

                if (OnSendCode != null) { OnSendCode(this, null); }

                Xaml_Code.IsEnabled = true;
                Xaml_Code.Focus();
                Xaml_UserName.IsEnabled = false;
            }
        }


        /// <summary>
        /// 创建四位英文字符验证码
        /// </summary>
        /// <returns></returns>
        string CreateEnglish(int length = 4)
        {
            Random rand = new Random();
            rand.Next(67, 97);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                if (new Random().Next(0, 2) == 0)
                {
                    char c = Convert.ToChar(rand.Next(97, 123));
                    sb.Append(c);
                }
                else
                {
                    char c = Convert.ToChar(rand.Next(65, 91));
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }


        /// <summary>
        /// 清除错误信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_UserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            ErrorInfo.Text = "";
        }

        /// <summary>
        /// 验证码框输入回车
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_Code_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (Xaml_Code.Text == "")
                {
                    ErrorInfo.Text = "还未输入验证码";
                }
                else CodeIsValid();
            }
        }
    }




    public partial class C_ResetPass_Black
    {
        /// <summary>
        /// 用户名输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_UserError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 用户名输入框丢失焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length == 0)
                Xaml_UserError.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 密码输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_NewPassError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 密码输入框焦点丢失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_NewPass.Text.Length == 0)
                Xaml_NewPassError.Visibility = Visibility.Visible;
        }


        /// <summary>
        /// 重置文本框状态
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetTextBox(object sender, RoutedEventArgs e)
        {
            Xaml_UserName.IsEnabled = true;
            Xaml_UserName.Text = ""; Xaml_UserName.Focus();//清理用户名

            Xaml_CodeError.Visibility = Visibility.Visible;
            Xaml_Code.Text = ""; Xaml_Code.IsEnabled = false;

            Xaml_NewPass.IsEnabled = false; Xaml_NewPass.Text = "";
            Xaml_NewPass.Visibility = Visibility.Collapsed;
            Xaml_NewPassError.Visibility = Visibility.Collapsed;
            Xaml_CodeInfo.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 验证码输入框焦点丢失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Code_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_Code.Text.Length < 1) { Xaml_CodeError.Visibility = Visibility.Visible; }
        }

        /// <summary>
        /// 验证码输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Code_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_CodeError.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 切换到输入新密码状态
        /// </summary>
        void GoInputNewPassState()
        {
            Xaml_UserName.IsEnabled = false;
            Xaml_CodeInfo.Visibility = Visibility.Collapsed;//隐藏验证码帮助信息
            Xaml_Code.IsEnabled = false;//禁用验证码输入框
            //Xaml_GetCode.Visibility = Visibility.Collapsed;//隐藏获取验证码按钮
            Xaml_NewPass.Visibility = Visibility.Visible;//显示新密码输入框
            Xaml_NewPass.IsEnabled = true;
            Xaml_NewPassError.Visibility = Visibility.Visible;//显示密码输入提示
            Xaml_Sub.Click -= Button_Click;
            Xaml_Sub.Click += Xaml_Sub_Click;
        }
    }
}
