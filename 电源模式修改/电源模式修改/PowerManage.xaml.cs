﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace 电源模式修改
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class PowerManage : Window
    {
        public PowerManage()
        {
            InitializeComponent();
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var PowerSchemes =  Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Power\User\PowerSchemes");
            Dictionary<string, string> powercfgs = Helper.GetPowerSchemes();
            var activeMode = Helper.GetPowerSchemes();
            //PowerSchemes.OpenSubKey("a1841308-3541-4fab-bc81-f71556f20b4a\\7516b95f-f776-4464-8c53-06167f40cc99\\aded5e82-b909-4619-9949-f5d71dac0bcb").SetValue("DCSettingIndex", 100);
            Xaml_PowerModes.DataContext = powercfgs;
            int index = 0;
            foreach (var item in powercfgs)
            {
                if (item.Key == activeMode.ToString())
                {
                    Xaml_PowerModes.SelectedIndex = index;
                    break;
                }
                index++;
            }
            var val = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Power\", "CsEnabled", "null").ToString();
            this.DataContext = new { IsAuto = val == "1" ? true : false };
        }

        /// <summary>
        /// 当控制台输出时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void pro_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(e.Data))
            {
                MessageBox.Show(e.Data);
            }
        }


        /// <summary>
        /// 点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as CheckBox;
            if(btn.IsChecked==false)
            {
                if (new Question().ShowDialog() == true)
                {
                    Helper.SetCsEnable(btn.IsChecked == true ? "1" : "0");
                    
                }
                else
                    btn.IsChecked = true;
            }
            else
                Helper.SetCsEnable(btn.IsChecked == true ? "1" : "0");
        }


        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 亮度修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LightValue_Changed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //System.Environment
        }

        /// <summary>
        /// 高级设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Advance_Click(object sender, RoutedEventArgs e)
        {
            AdvancePower w = new AdvancePower();
            w.Show();
        }

        /// <summary>
        /// 阻止在滑动条上拖动窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Slider_MouseMove_1(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// 选择项更改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Xaml_PowerModes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var value = Xaml_PowerModes.SelectedValue as string;
            if (string.IsNullOrWhiteSpace(value))
                return;
            var pro = new System.Diagnostics.Process() { };
            pro.StartInfo.RedirectStandardInput = true;
            pro.StartInfo.RedirectStandardOutput = true;
            pro.StartInfo.UseShellExecute = false;
            pro.StartInfo.CreateNoWindow = true;
            pro.StartInfo.FileName = "powercfg";
            pro.StartInfo.Arguments = "s " + value;
            pro.OutputDataReceived += pro_OutputDataReceived;
            pro.Start();
            pro.BeginOutputReadLine();
        }

        private void System_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("mblctr");
        }
    }
}
