﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.RegularExpressions;

namespace Dll_WPFControl
{
    /// <summary>
    /// UserRegister.xaml 的交互逻辑
    /// </summary>
    public partial class C_UserRegister_Black : UserControl
    {
        public C_UserRegister_Black()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName
        {
            get { return this.Xaml_UserName.Text; }
            set
            {
                this.Xaml_UserName.Text = value;
                if (value != null && value.Length > 1) { this.Xaml_UserError.Visibility = Visibility.Collapsed; }
            }
        }
        /// <summary>
        /// 密码文本
        /// </summary>
        public string PassWord
        {
            get { return this.Xaml_UserPass.Text.Trim(); }
        }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        public string Email 
        {
            set { this.Xaml_UserEmail.Text = value; }
            get { return this.Xaml_UserEmail.Text; }
        }

        /// <summary>
        /// 注册完成时的操作
        /// </summary>
        public event RoutedEventHandler OnCompleted = null;

        /// <summary>
        /// 跳转到登陆时的操作
        /// </summary>
        public event RoutedEventHandler OnGoLogin = null;


        /// <summary>
        /// 用户名输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_UserError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 用户名输入框丢失焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length == 0)
                Xaml_UserError.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 密码输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_PassError.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 密码输入框焦点丢失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPass_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserPass.Text.Length == 0)
                Xaml_PassError.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 注册点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserName.Text.Length < 5)
            {
                ErrorInfo.Text = "用户名至少需要5位";
            }
            else if (Xaml_UserPass.Text.Length < 6)
            {
                ErrorInfo.Text = "用户密码至少6位";
            }
            else
            {
                Regex r = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
                Match m = r.Match(this.Xaml_UserEmail.Text);
                if (m != Match.Empty)
                {
                    if (OnCompleted != null) { OnCompleted(this, null); }
                }
                else 
                {
                    ErrorInfo.Text = "输入的邮箱地址无效！";
                }
            }
        }

        /// <summary>
        /// 跳转到登陆界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Go_UserLogin(object sender, RoutedEventArgs e)
        {
            if (OnGoLogin != null) OnGoLogin(this,null);
        }

        /// <summary>
        /// 邮箱输入框获得焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserEmail_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_EmailError.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 邮箱输入框丢失焦点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Xaml_UserEmail.Text == "") Xaml_EmailError.Visibility = Visibility.Visible;
        }

        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            ErrorInfo.Text = "";
        }

    }
}
