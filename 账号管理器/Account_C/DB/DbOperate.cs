﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{

    /// <summary>
    /// 数据库操作提供
    /// </summary>
    public class DbOperate
    {
        /// <summary>
        /// 测试数据库可用性
        /// </summary>
        /// <returns></returns>
        public bool TestDbConnection()
        {
            using (var db = DAL.DbConnects.GetDbHelper())
            {
                try
                {
                    db.DBOpen();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 复制到新的数据库
        /// </summary>
        /// <param name="newDbConnectionString"></param>
        /// <returns></returns>
        public bool CopyToNewDB(string newDbConnectionString)
        {
            try
            {
                var drs = Get("");
                List<Models.Account> accounts = new List<Models.Account>();
                DataTable dt = new DataTable();
                dt.Columns.Add("AccountGUID");
                dt.Columns.Add("UserID");
                dt.Columns.Add("SiteUid");
                dt.Columns.Add("Alias");
                dt.Columns.Add("SitePwd");
                dt.Columns.Add("SiteTitle");
                dt.Columns.Add("SiteUrl");
                dt.Columns.Add("ImagePath");
                dt.Columns.Add("Remark");
                dt.Columns.Add("State");
                foreach (var item in drs)
                {
                    dt.Rows.Add(new object[]
                {
                    item.AccountGUID,
                    item.UserID,
                    item.SiteUid,
                    item.Alias,
                    item.SitePwd,
                    item.SiteTitle,
                    item.SiteUrl,
                    item.ImagePath,
                    item.Remark,
                    item.State,
                    });
                }
                List<DataRow> drss = new List<DataRow>();
                foreach (DataRow item in dt.Rows)
                {
                    drss.Add(item);
                }
                DAL.SQLDBHelper.BulkCopy(drss.ToArray(), "Account", newDbConnectionString);
                return true;
            }
            catch {
                return false;
            }
        }
        
        public List<Models.Account> Get(string userID)
        {
            List<Models.Account> Accounts = new List<Models.Account>();
            using (var db = DbConnects.GetDbHelper()) 
            {
                using (var dr = db.ExecuteReader("select * from Account order by SiteTitle")) 
                {
                    while (dr.Read())
                    {
                        Accounts.Add(new Models.Account()
                        {
                            AccountGUID = dr["AccountGUID"] as string,
                            Alias = dr["Alias"] as string,
                            ImagePath = dr["ImagePath"] as string,
                            Remark = dr["Remark"] as string,
                            SitePwd = dr["SitePwd"] as string,
                            SiteTitle = dr["SiteTitle"] as string,
                            SiteUid = dr["SiteUid"] as string,
                            SiteUrl = dr["SiteUrl"] as string,
                            State = (int)dr["State"]
                            //,
                            //TypeGuid = dr["TypeGuid"] as string,
                            //TypeName = dr["TypeName"] as string,
                            //UserID = dr["UserID"] as string
                        });
                    }
                }
            }
            return Accounts;
        }

        public int AddAccount(Models.Account accountInfo)
        {
            using (var db = DAL.DbConnects.GetDbHelper())
            {
                return db.ExecuteNonQuery("insert into Account(Alias,ImagePath,Remark,SitePwd,SiteTitle,SiteUid,SiteUrl,State,UserID) values(@alias,@image,@remark,@pwd,@title,@uid,@url,@state,@user)", ToSqlParameter(accountInfo));
            }
        }

        public int Delete(Models.Account accountInfo)
        {
            return DAL.DbConnects.GetDbHelper().ExecuteNonQuery("delete Account where accountGUID = @guid",new SqlParameter("@guid",accountInfo.AccountGUID??""));
        }

        public int UpdateAccount(Models.Account accountInfo)
        {
            using (var db = DAL.DbConnects.GetDbHelper())
            {
                return db.ExecuteNonQuery("update Account set Alias=@alias,ImagePath=@image,Remark=@remark,SitePwd=@pwd,SiteTitle=@title,SiteUid=@uid,SiteUrl=@url,State=@state,UserID=@user where AccountGUID=@guid", ToSqlParameter(accountInfo));
            }
        }

        /// <summary>
        /// 将模型转换成参数
        /// </summary>
        /// <param name="accountInfo"></param>
        /// <returns></returns>
        public SqlParameter[] ToSqlParameter(Models.Account accountInfo)
        {
            return new System.Data.SqlClient.SqlParameter[]{
                    new System.Data.SqlClient.SqlParameter("@guid",accountInfo.AccountGUID??""),
                    new System.Data.SqlClient.SqlParameter("@alias",accountInfo.Alias??""),
                    new System.Data.SqlClient.SqlParameter("@image",accountInfo.ImagePath??""),
                    new System.Data.SqlClient.SqlParameter("@remark",accountInfo.Remark??""),
                    new System.Data.SqlClient.SqlParameter("@pwd",accountInfo.SitePwd??""),
                    new System.Data.SqlClient.SqlParameter("@title",accountInfo.SiteTitle??""),
                    new System.Data.SqlClient.SqlParameter("@uid",accountInfo.SiteUid??""),
                    new System.Data.SqlClient.SqlParameter("@url",accountInfo.SiteUrl??""),
                    new System.Data.SqlClient.SqlParameter("@state",accountInfo.State),
                    //new System.Data.SqlClient.SqlParameter("",accountInfo.TypeGuid),
                    //new System.Data.SqlClient.SqlParameter("",accountInfo.TypeName),
                    new System.Data.SqlClient.SqlParameter("@user",accountInfo.UserID??"")
                };
        }

    }
}
