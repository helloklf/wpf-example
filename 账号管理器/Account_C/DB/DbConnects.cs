﻿using System.Configuration;
using System.Data.SqlClient;

namespace DAL
{
    public class DbConnects
    {
        static string connText= ConfigurationManager.ConnectionStrings["LocalDB"].ConnectionString;
        internal static SqlConnection sqlConn = new SqlConnection(connText);//SQL数据库使用的连接方式

        /// <summary>
        /// 创建Sql连接实例(DBHelper是SQLDBHelper的父类，如果你要使用其它数据库，也可改成DBHelper的其它子类)
        /// </summary>
        /// <returns></returns>
        public static DBHelper GetDbHelper()
        {
            return new SQLDBHelper(DbConnects.sqlConn);
        }

        /// <summary>
        /// 重新创建连接
        /// </summary>
        public static void CreateConn(string conneText=null)
        {
            sqlConn = new SqlConnection( connText??connText);
        }
    }
}
