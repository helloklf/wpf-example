﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Dll_WPFControl
{
    /// <summary>
    /// C_SearchBox_Black：黑色文本的搜索框
    /// </summary>
    public partial class C_SearchBox_Black : UserControl
    {
        public C_SearchBox_Black()
        {
            InitializeComponent();
            //绑定建议列表
            this.Xaml_DownList.DataContext = SuggestList;
        }

        /// <summary>
        /// 搜索时触发
        /// </summary>
        public event RoutedEventHandler Click = null;

        List<string> data = new List<string>();

        /// <summary>
        /// 搜索建议数据源（＂List<String>＂类型>）
        /// </summary>
        public List<string> Data
        {
            get { return data; }
            set
            {
                data = value;
                ResetDownList();
            }
        }

        ObservableCollection<string> suggestList = new ObservableCollection<string>();
        ObservableCollection<string> SuggestList
        {
            get
            {
                return suggestList;
            }
        }

        /// <summary>
        /// 输入框的文本
        /// </summary>
        public string Text
        {
            set
            {
                InputContent.Text = value;
                ResetDownList();
            }
            get
            {
                return InputContent.Text;
            }
        }

        /// <summary>
        /// 搜索按钮点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Click != null) { Click(this, new RoutedEventArgs()); }
        }

        /// <summary>
        /// 进入输入框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputContent_GotFocus(object sender, RoutedEventArgs e)
        {
            Xaml_watermark.Opacity = 0;
            Xaml_DownList.Visibility = Visibility.Visible;
            ResetDownList();
        }

        /// <summary>
        /// 离开输入框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputContent_LostFocus(object sender, RoutedEventArgs e)
        {
            if (InputContent.Text == "") { Xaml_watermark.Opacity = 0.5; }
            Xaml_DownList.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// 回车时搜索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputContent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) { if (Click != null) { Click(this, new RoutedEventArgs()); } }
        }

        /// <summary>
        /// 选择搜索建议
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var select = Xaml_DownList.SelectedValue as string;
            if (select == null || select.Length < 1) return;

            Text = Xaml_DownList.SelectedValue as string;
            if (Click != null)
                Click(this, new RoutedEventArgs());
            InputContent.Focus();
        }

        private void InputContent_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void InputContent_KeyUp(object sender, KeyEventArgs e)
        {
            ResetDownList();
        }

        /// <summary>
        /// 刷新下拉列表建议条目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ResetDownList()
        {
            if (Data == null) return;
            string t = InputContent.Text.Trim();
            suggestList.Clear();
            foreach (var item in (from a in Data where a.ToLower().Contains(t.ToLower()) select a).ToList())
            {
                suggestList.Add(item);
            }
        }
    }
}
