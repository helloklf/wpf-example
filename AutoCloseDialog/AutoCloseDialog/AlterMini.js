﻿//显示小的提示框（文本内容（支持HTML标记），自动关闭时间（ms）：默认为5000毫秒）
function AlterMini(text, timeout) {
    timeout = timeout ? timeout : 5000;

    var blockId = randomChar(10);

    function CloseBlock() {
        var block = document.getElementById(blockId);
        block.parentNode.removeChild(block);
    }
    function OpenBlock() {
        var RootPanelID = randomChar(20);//随机创建一个id

        if (document.body) document.body.innerHTML += HtmlContent;
        else document.write(HtmlContent);
        setTimeout(CloseBlock, timeout);
        document.getElementById(blockId + "_close").onclick = CloseBlock;
    }
    function randomChar(l) { //随机字符串创建器
        var x = "123456789poiuytrewqasdfghjklmnbvcxzQWERTYUIPLKJHGFDSAZXCVBNM";
        var tmp = "";
        for (var i = 0; i < l; i++) {
            tmp += x.charAt(Math.ceil(Math.random() * 100000000) % x.length);
        }
        return tmp;
    }
    var HtmlContent = "<div class='MiniDialogBox' id='" + blockId + "'><style>.MiniDialogBox{border:1px solid#e5e5e5;display:inline-block;width:auto;min-width:200px;background:#f5f5f5;max-width:300px;position:fixed;z-index:99998;top:20%;left:40%;}.SDialogBoxBodyText{line-height:1.8em;font-weight:400;font-weight:600;display:inline-block;vertical-align:top;padding:10px;font-size:14px;}.SDialogBoxBodyImg{width:32px;margin:10px;height:32px;border-radius:16px;background:#0078ad;}.SDialogBoxbtnBlock{font-size:9px;cursor:pointer;color:#fff;border:1px solid#f5f5f5;background:#e5e5e5;font-weight:600;line-height:20px;text-align:center;width:18px;height:18px;display:inline-block;border-radius:2px;vertical-align:top;}.btnBlock:hover{background:#c5c5c5;}</style><table style='width:100%; border-collapse:collapse; margin:3px 0px;'><tr><td style='width:auto; vertical-align:top'><img class='SDialogBoxBodyImg'src='" + Icon_Check + "'/></td><td><div class='SDialogBoxBodyText'>" + text + "</di></td><td style='vertical-align:top; width:22px; padding:0px;'><img id='" + blockId + "_close' class='SDialogBoxbtnBlock'src='" + Icon_Close + "'/></td></tr></table></div>";
    OpenBlock();
}

//Base64图片数据：关闭（“X”按钮）
var Icon_Close = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAYAAADHl1ErAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFkSURBVHhe7dpBToNAFIDhYmDhLVx4EPduXLk2HsHUe3kEL6ULYaxvZF5iqLUzMBhm3v8lhAyhNO9vStKUHQAAAAAAAAAAAPB/hmF4lO0uLLPp+/7GOfcUlnXwsT5HHzmj+VhyzbfD6DkcLpvGCkMdckWbxFJlR5vGUkujnYilyowmQR5+i6XmRjsT65vc0/bh9HLEDJYabY1rbkrOAauPpXIMaiaWWjKwuVhqzuBmY6mUAOZjqdgQxPohJtpfTMVSc6OZjKVSo5mOpWKjbSXWRdijBHwlE3DTTzA3ljIVLSaWDxJzTvXRYmP5ECnnhsvXZU4As9GWDG4uWo6BzUTLOWj10dYYcAvRVvtp1DTNlewux9Uxma2X4e7btn0Jh87quu7VOXcrr30Ph47I+3ayXYdlWeST5o/cVNNoS2OpE9Hqer5CZL2/TKLVEUv5aDljKR+tusedAAAAAAAAAAAAsGm73Rda1br6wfcBoQAAAABJRU5ErkJggg==";

//Base64图片数据：确定（“✅”按钮）
var Icon_Check = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEwAAABMCAYAAADHl1ErAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAI6SURBVHhe7dg/SBthGMfxIKUgpSCCk0uHUuji6Ojm0snFzbVTly4d2ilbcXN2cHNxF8dQR3GTgJtTIW2GEgohmH9+37vnIKnvXXImFd57fx843ph77vXeH7k77qmJiIiIiIiIiIiIiIiIVMt4PN4YDoeH9Xp9xb6SPM1m8yWB/WAbE9qJQpuBkI5cWBmFVoB89tOYpik0D3LZYusmCXm40Kx0KYJOv91uv2Y4Y1tNvvD7ZWPc3KU2Go0u7IeU57zRaLywQ+JGGN/STPwI84ZhzcrjRhAfCGSQJOP3u9frvbXyuBHGG7Y/SSweBHnPsGvlcWu1Wq8I5CqNxo+n4icrD487eS6Nd/bnwpjv1HLJc2yl4eHkv7gV8Iv4uYzQmOqjmy8P/+fSvR5ZeVg4/ySszKKh9fv9Hbs35bnrdDrrVh4WTn4qrMxTQ+PQDXdsOstj7PvL8N7Kw8KJe8PKlA1tsgPhw3yjwWCwZ+XhYQ2FgTllQuMmP9WB+Bf7v1ppuFjHUkKjzNuBmODeIauBxSwUGrsLOxAce81Q9MIdHhb0pNBcB4Jdt2nFY+6Ybre7aeXVwvpKhTarA8G+e2wnk1cV65w7ND4WdiBwYNNWGwudGRpaBJfbgeCJ+N2miwNrnie0PHE2All46dD41cXdCGTxZUJTI9AhiHkeBGoETiKMwtBcL81KJUMueaGF2wj83whnKjQuxXAbgc+FnLLQwm0EPjfuWZ8JLMxGoIiIiIiIiIiIiIiIiESlVnsAVoe0Wymm6WkAAAAASUVORK5CYII=";