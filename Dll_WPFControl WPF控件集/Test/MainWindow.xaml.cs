﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //此操作为搜索框下拉列表提供搜索建议
            C_SearchBox.Data = new List<string>
            {
                "ol,adfsadosfcaoifdsd ",
                "的十大歌手唱歌是个发生",
                "dsgsdagasdg",
                "受到损失大公司打工",
                "是的住房市场分析师地方vs",
                "vdfgsfcgsfdcgsf",
                "啊大方向是对西方的撒非常大沙发",
                "岁的法国选手的方向搭顺风车的撒"
            };
        }


        /// <summary>
        /// 搜索按钮点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchBox_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("搜索内容："+ C_SearchBox.Text );
        }

        #region 用户综合操作模块

        /// <summary>
        /// 登陆完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserOperate_OnLoginCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户：" + this.C_UserOperate.UserName + "密码：" + this.C_UserOperate.UserPass);
        }

        /// <summary>
        /// 注册完毕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserOperate_OnRegisterCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户：" + this.C_UserOperate.UserName + "密码：" + this.C_UserOperate.UserPass + "邮箱：" + this.C_UserOperate.UserEmali);
        }

        /// <summary>
        /// 密码找回完毕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserOperate_OnResetPassCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户：" + this.C_UserOperate.UserName + "新密码：" + this.C_UserOperate.UserPass);
        }
        #endregion

        #region 登陆模块
        /// <summary>
        /// 登陆完成时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserLogin_OnCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户名："+this.C_UserLogin.UserName+"　密码："+this.C_UserLogin.PassWord);
        }

        /// <summary>
        /// 跳转到注册界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_OnGoRegister(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("还没有注册，快去注册吧！");
        }

        /// <summary>
        /// 转到重置密码操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserLogin_OnGoResetPass(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("忘记了密码，快去重置密码！");
        }
        #endregion

        #region 注册模块
        /// <summary>
        /// 注册完成时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserRegister_OnCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户名：" + C_UserRegister.UserName + "\r\n密码：" + C_UserRegister.PassWord + "\r\n邮箱：" + C_UserRegister.Email);
        }

        /// <summary>
        /// 转到登陆界面操作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_UserRegister_OnGoLogin(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("我已经有账号了，快去登陆！");   
        }
        #endregion

        #region 密码重置模块

        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_PassReset_OnSendCode(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户：" + C_PassReset.UserName + "\t应当通过邮箱收到验证码：" + C_PassReset.CodeText);
        }
        /// <summary>
        /// 密码修改完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void C_ResetPass_OnCompleted(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("用户" + C_PassReset.UserName + "　的密码修改为：" + C_PassReset.NewPassWord);
        }
        #endregion

    }
}
