﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Xml.Serialization;

namespace Windows服务安装工具
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 安装
        /// </summary>
        private void Install_Click(object sender, RoutedEventArgs e)
        {
            Run("");   
        }

        /// <summary>
        /// 卸载
        /// </summary>
        private void UInstall_Click(object sender, RoutedEventArgs e)
        {
            Run("/u");
        }

        /// <summary>
        /// 选择文件
        /// </summary>
        private void FileName_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SelectFile();            
        }

        /// <summary>
        /// 选择文件
        /// </summary>
        private void FileSelect_Click(object sender, RoutedEventArgs e)
        {
            SelectFile();
        }

        private void InstallStateOutput_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as TextBox).ScrollToEnd();
        }

        private void InstallStateOutput_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Xaml_HistoryList_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (Xaml_HistoryList.SelectedValue!=null)
                FileName.Text = Xaml_HistoryList.SelectedValue as string;
        }

        private void HistoryOpen_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_HistoryList.Visibility == Visibility.Collapsed)
            {
                LoadToList();
                (sender as Control).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF9D59CD"));
                Xaml_HistoryList.Visibility = Visibility.Visible;
                InstallStateOutput.Visibility = Visibility.Collapsed;
            }
            else
            {
                (sender as Control).Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF97662B"));
                Xaml_HistoryList.Visibility = Visibility.Collapsed;
                InstallStateOutput.Visibility = Visibility.Visible;
            }
        }


        List<string> HistoryFile = new List<string>();

        void LoadToList()
        {
            Xaml_HistoryList.ItemsSource = null;
            Xaml_HistoryList.ItemsSource = HistoryFile;
        }

        void Save()
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(HistoryFile.GetType());
                using (var stream = File.Open("History.log", FileMode.Create))
                {
                    xs.Serialize(stream, HistoryFile);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void Load()
        {
            try
            {
                if (File.Exists("History.log"))
                {
                    XmlSerializer xs = new XmlSerializer(HistoryFile.GetType());
                    using (var stream = File.Open("History.log", FileMode.OpenOrCreate))
                    {
                        HistoryFile = (xs.Deserialize(stream)) as List<string>;
                    }
                    if (HistoryFile != null && HistoryFile.Count > 0)
                    {
                        this.FileName.Text = HistoryFile[HistoryFile.Count - 1] ?? "请点 ➕添加 按钮选择要【安装/卸载】的Windows服务（EXE文件）";
                    }
                    Xaml_HistoryList.ItemsSource = HistoryFile;
                }
            }
            catch (Exception ex)
            {
                HistoryFile = new List<string>();
                Save();
                MessageBox.Show(ex.Message);
            }
        }

        void SelectFile()
        {
            Microsoft.Win32.OpenFileDialog of = new Microsoft.Win32.OpenFileDialog() { Filter = "可执行文件|*.exe" };
            if (of.ShowDialog() == true)
            {
                this.FileName.Text = of.FileName;
                if (HistoryFile == null)
                    HistoryFile = new List<string>();
                if (!HistoryFile.Contains(of.FileName))
                    HistoryFile.Add(of.FileName);
                else
                {
                    HistoryFile.Remove(of.FileName);
                    HistoryFile.Add(of.FileName);
                }

                LoadToList();

                Save();
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        /// <summary>
        /// 选择参数执行
        /// </summary>
        /// <param name="Arguments"></param>
        void Run(string Arguments = "")
        {
            if (File.Exists(FileName.Text))
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(@"C:\\windows\Microsoft.NET\Framework\v4.0.30319\Installutil.exe ");
                info.Arguments = FileName.Text + " " + Arguments;
                info.RedirectStandardInput = true;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                Process process = Process.Start(info);

                process.OutputDataReceived += (a, b) =>
                {
                    if (string.IsNullOrWhiteSpace(b.Data))
                        return;
                    this.Dispatcher.Invoke(() => { this.InstallStateOutput.Text += (b.Data + "\r\n"); });
                };
                process.BeginOutputReadLine();

            }
            else
            {
                MessageBox.Show("所选文件不存在，请重新选择要安装的文件！");
            }
        }

        /// <summary>
        /// 移除项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveSelect_Click(object sender, RoutedEventArgs e)
        {
            if (Xaml_HistoryList.SelectedValue != null && HistoryFile != null)
                HistoryFile.Remove((Xaml_HistoryList.SelectedValue as string));
            LoadToList();
            Save();
        }

        /// <summary>
        /// 清空历史记录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearAll_Click(object sender, RoutedEventArgs e)
        {
            HistoryFile = new List<string>();
            LoadToList();
            Save();
        }
    }
}
