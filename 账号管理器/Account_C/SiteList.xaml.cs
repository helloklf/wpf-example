﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Account_C
{
    /// <summary>
    /// SiteList.xaml 的交互逻辑
    /// </summary>
    public partial class SiteList : UserControl
    {
        List<Models.Account> Data;
        string searchText="";
        public SiteList()
        {
            InitializeComponent();
            
            //设置刷新方法
            Delegates.Reference = new Reload(() => 
            {
                Accounts.Clear();
                Data = new DAL.DbOperate().Get("");
                Delegates.OnReferenced(Data);
                Delegates.Search(searchText);
            });

            //设置搜索方法
            Delegates.Search = new Search((a) =>
            {
                Accounts.Clear();
                if (Data != null)
                foreach (var item in Data)
                {
                    if (item.SiteTitle != null && a != null && item.SiteTitle.ToLower().Contains(a.Trim().ToLower()))
                        Accounts.Add(item);
                }
                searchText = a;
            });

            //设置删除操作
            Delegates.OnDeleting = new OnEdit((a) =>
            {
                if (MessageBox.Show("您真的要删除这条操作吗？删除以后将无法找回。", "确定删除", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    new DAL.DbOperate().Delete(a);
                    if (Delegates.Reference != null)
                        Delegates.Reference();
                }
            });

            SiteListBox.DataContext = Accounts;
        }

        System.Collections.ObjectModel.ObservableCollection<Models.Account> Accounts = new System.Collections.ObjectModel.ObservableCollection<Models.Account>();

        /// <summary>
        /// 加载时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Delegates.Reference != null)
                Delegates.Reference();
            else
                MessageBox.Show("系统出现问题，请手动点击右上角的刷新按钮!");
        }
    }
}
