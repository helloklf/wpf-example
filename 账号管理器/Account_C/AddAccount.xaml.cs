﻿using Account_C.DB;
using Microsoft.Win32;
using System;
using System.IO;
using System.Security.AccessControl;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Account_C
{
    /// <summary>
    /// AddAccount.xaml 的交互逻辑
    /// </summary>
    public partial class AddAccount : UserControl
    {
        public AddAccount()
        {
            InitializeComponent();
            this.DataContext = account;
        }

        Models.Account account = new Models.Account() { SiteUrl = "http://" };
        /// <summary>
        /// 取消修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if (Delegates.OnEdited != null)
                Delegates.OnEdited(null);
        }

        /// <summary>
        /// 完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Complate_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists("HeaderImages"))
                Directory.CreateDirectory("HeaderImages");
            if (account.ImagePath!=null&&File.Exists(account.ImagePath))
            {
                FileInfo fi = new FileInfo(account.ImagePath);
                var filepath = "HeaderImages\\" + Guid.NewGuid().ToString()+fi.Extension;
                File.Copy(account.ImagePath, filepath);
                account.ImagePath = filepath;
            }
            try
            {
                new DAL.DbOperate().AddAccount(this.DataContext as Models.Account);
            }
            catch(Exception ex)
            {
                DbError.Update(ex);
                return;
            }
            if (Delegates.OnEdited != null)
                Delegates.OnEdited(account);
            else
            {
                MessageBox.Show("添加完成");
            }
            DataContext = new Models.Account() { SiteUrl = "Http://" };
            
        }

        /// <summary>
        /// 更换图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectImage_Click(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "图片文件|*.png;*.jpg;*.gif;*.bmp;*.Jpeg|所有文件|*.*";
            if (of.ShowDialog()==true)
            {
                this.Xaml_SiteImg.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(of.FileName);
                account.ImagePath = of.FileName;
            }
        }
    }
}
