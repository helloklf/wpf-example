﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OmAreaStore
{
    class ProductInfo
    {
        string name;
        string remark;
        string price;
        string downloadUrl;

        /// <summary>
        /// 下载地址
        /// </summary>
        public string DownloadUrl
        {
            get { return downloadUrl; }
            set { downloadUrl = value; }
        }
        /// <summary>
        /// 价格
        /// </summary>
        public string Price
        {
            get { return price; }
            set { price = value; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }
}
