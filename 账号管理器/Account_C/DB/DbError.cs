﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Account_C.DB
{
    public static class DbError
    {
        public static void Update(Exception ex)
        {
            if (ex.Message.Contains("the database is read-only"))
            {
                MessageBox.Show("让应用程序自动修复此错误？", "无权数据库内容",MessageBoxButton.OK,MessageBoxImage.Error);

                SetFileControl.SetFilesControl(new string[] { "LocalDB.mdf", "LocalDB_log.ldf" });

                MessageBox.Show("修复完毕，请重新启动应用程序！如果再次出现此错误，请手动设置安装目录下的LocalDB.mdf、LocalDB_log.ldf的Everyone访问权限为'完全控制");
                return;
            }
            MessageBox.Show("访问数据库失败，！");
        }
    }
}
