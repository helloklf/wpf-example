﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OmAreaStore
{
    /// <summary>
    /// UserControl1.xaml 的交互逻辑
    /// </summary>
    public partial class StoreList : UserControl
    {
        public StoreList()
        {
            InitializeComponent();
            /*this.ProductsList.DataContext = new List<ProductInfo>() {
                new ProductInfo(){ Name="账号管理器",DownloadUrl="Http://www.baidu.com",Price="￥0.00",Remark="一款管理您的账号密码的贴心小软件" },
            };*/
        }

        WebBrowser web;
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            web = new WebBrowser();
            ProductsList_Panel.Children.Add(web);
        }
    }
}
